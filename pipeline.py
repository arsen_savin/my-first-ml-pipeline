# import a dataset
from sklearn import datasets
iris = datasets.load_iris()

X = iris.data
y = iris.target

# partition into training test
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = .5)

# import Decision Tree classifier
from sklearn import tree
dt_clf = tree.DecisionTreeClassifier()

# import K Nearest Neighbors classifier
from sklearn.neighbors import KNeighborsClassifier
knn_clf = KNeighborsClassifier()

# training Decision Tree classifier using training data
dt_clf.fit(X_train, y_train)

# training K Nearest Neighbors classifier using training data
knn_clf.fit(X_train, y_train)

# Decision Tree predictions
dt_clf_predictions = dt_clf.predict(X_test)
print dt_clf_predictions

# K Nearest Neighbors predictions
knn_clf_predictions = knn_clf.predict(X_test)
print knn_clf_predictions

# checking accuracy of predictions with Decision Tree classifier
from sklearn.metrics import accuracy_score
print accuracy_score(y_test, dt_clf_predictions)

# checking accuracy of predictions with K Nearest Neighbors classifier
print accuracy_score(y_test,knn_clf_predictions)
